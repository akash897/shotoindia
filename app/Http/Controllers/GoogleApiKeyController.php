<?php

namespace App\Http\Controllers;

use App\GoogleApiKey;
use Illuminate\Http\Request;

class GoogleApiKeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GoogleApiKey  $googleApiKey
     * @return \Illuminate\Http\Response
     */
    public function show(GoogleApiKey $googleApiKey)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GoogleApiKey  $googleApiKey
     * @return \Illuminate\Http\Response
     */
    public function edit(GoogleApiKey $googleApiKey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GoogleApiKey  $googleApiKey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GoogleApiKey $googleApiKey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GoogleApiKey  $googleApiKey
     * @return \Illuminate\Http\Response
     */
    public function destroy(GoogleApiKey $googleApiKey)
    {
        //
    }
}
