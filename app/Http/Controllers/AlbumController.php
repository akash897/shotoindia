<?php

namespace App\Http\Controllers;

use App\Album;
use App\Event;
use Illuminate\Http\Request;
use App\Image;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($album)
    {
        $event_data = Event::where('key',$album)->get();
        if(!empty($event_data[0])){
            $event = $event_data[0];
            $albums = $event->album;
            $album_name = $event->name;
            return view('page.album',compact('albums','album_name','event'));
        }
        else
            abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_events = Event::all();
        return view('admin.album.create', compact('all_events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param $event_key
     * @param $album_key
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($event_key, $album_key)
    {
        $event_data = Event::where('key',$event_key)->get();
        if(!empty($event_data[0])){
            $event = $event_data[0];
            $album_data = Album::where(['event_id'=>$event->id,'key'=>$album_key])->get();
            $album = $album_data[0];
            if($album->key == $album_key){
                $images = $album->image;
                $album_detail = Album::findorfail($album->id);
                $image_album_name = $album_detail->name;
                return view('page.dataimage',compact('images','image_album_name'));
            }
            else
                abort(404);
        }else
            abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploading(Request $request,$id)
    {
        $event = Event::findorfail($id);
        if(!empty($event)){
            $image = $request->file;
            $url_name = $image->getClientOriginalName();
            $new_name = $url_name;
            $smallthumbnail = 'small_'.$new_name;
            $mediumthumbnail = 'medium_'.$new_name;
            $largethumbnail = 'large_'.$new_name;
            $request->file->storeAs("public/upload_folder/{$event->name}", $new_name);

            $request->file->storeAs("public/upload_folder/{$event->name}/thumbnail", $smallthumbnail);
            $request->file->storeAs("public/upload_folder/{$event->name}/thumbnail", $mediumthumbnail);
            $request->file->storeAs("public/upload_folder/{$event->name}/thumbnail", $largethumbnail);

            //create small thumbnail
//            $smallthumbnailpath = public_path("upload_folder/{$event->name}/thumbnail".$smallthumbnail);
//            $this->createThumbnail($smallthumbnailpath, 150, 93);

            //create medium thumbnail
//            $mediumthumbnailpath = public_path("upload_folder/{$event->name}/thumbnail".$mediumthumbnail);
//            $this->createThumbnail($mediumthumbnailpath, 300, 185);

            //create large thumbnail
//            $largethumbnailpath = public_path("upload_folder/{$event->name}/thumbnail".$largethumbnail);
//            $this->createThumbnail($largethumbnailpath, 550, 340);
//
            $album = new Album();
            $album->image_name = $new_name;
            $album->image_url = "images/upload_folder/{$event->name}/{$url_name}";
            $album->event_id = $id;
            $album->save();

            return response()->json([
                'status' => 'Success',
                'status_code' => '200'
            ]);
        }
        else{
            return response()->json([
                'status' => 'Un-Success',
                'read' => '500'
            ]);
        }
    }

    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }
}
