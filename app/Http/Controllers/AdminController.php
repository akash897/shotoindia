<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use phpDocumentor\Reflection\Types\Array_;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dashboard(){
        return view('admin.dashboard');
    }

    public function profile(){
        $agent = new Agent();
        $agent_data = new Array_();
        $agent_data->is_mobile = $agent->isMobile();
        $agent_data->is_tablet = $agent->isTablet();
        $agent_data->languages = $agent->languages();
        $agent_data->device = $agent->device();
        $agent_data->is_desktop = $agent->isDesktop();
        $agent_data->is_iphone = $agent->isPhone();
        $agent_data->is_robot = $agent->isRobot();
        $agent_data->robot_name = $agent->robot();
        $agent_data->browser = $agent->browser();
        $agent_data->browser_version = $agent->version($agent->browser());
        $agent_data->platform = $agent->platform();
        $agent_data->platform_version = $agent->version($agent->platform());
//        dd($agent_data);
        return view('admin.profile.profile',compact('agent_data'));
    }
}
