<?php

namespace App\Http\Controllers;

use App\BlackBelt;
use App\BlackBest;
use App\DanList;
use App\Instructor;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class BlackBeltController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $values = BlackBelt::withTrashed()->orderBy('id', 'asc')
                    ->where('instructor','no')
                    ->get();
        return view('admin.instructor.blackbelt-list',compact('values'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $year = [];
        $current_year = Carbon::now()->year;
        for($i = $current_year ; $i >= 1950 ; $i-- ){
            $year[] = $i;
        }
        return view('admin.instructor.add-blackbelts',compact('year'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blackbelt = new BlackBelt();
        $blackbelt->full_name = $request->fullname;
        $blackbelt->dan_list_id = $request->dan;
        $blackbelt->year = $request->year;

        if(Input::hasFile('name')){

            $file = Input::file('name');
            $file->move('uploads/instructors', $file->getClientOriginalName());
            $blackbelt->image_url = 'uploads/instructors/'.$file->getClientOriginalName();
        }

        $blackbelt->created_by = Auth::user()->name;
        $blackbelt->modified_by = Auth::user()->name;
        $blackbelt->save();
        return redirect()->route('blackbelt-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlackBest  $blackBest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $value = BlackBelt::withTrashed()->findorfail($id);
        return view('admin.instructor.blackbelt-view',compact('value'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlackBest  $blackBest
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = BlackBelt::findorfail($id);
        $dan_list = DanList::all();
        $year = [];
        $current_year = Carbon::now()->year;
        for($i = $current_year ; $i >= 1950 ; $i-- ){
            $year[] = $i;
        }
        return view('admin.instructor.blackbelt-edit',compact('user','dan_list','year'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlackBest  $blackBest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = BlackBelt::findorfail($id);

        if(!empty($user)){

            $instructor = Instructor::where('black_belt_id',$id)->get();

            if(empty($instructor[0])){
                $new_instructor = new Instructor();
                $new_instructor->black_belt_id = $user->id;
                $new_instructor->save();
            }

            $user->full_name = $request->fullname;
            $user->dan_list_id = $request->dan;
            $user->instructor = $request->instructor;
            $user->year = $request->year;

            if(Input::hasFile('name')){

                if(!empty($user->image)){
                    $pathOfImage = public_path().'/uploads/instructors/'.$user->image_url;
                    File::delete($pathOfImage);
                }

                $file = Input::file('name');
                $file->move('uploads/instructors', $file->getClientOriginalName());
                $user->image_url = 'uploads/instructors/'.$file->getClientOriginalName();
            }

            $user->modified_by = Auth::user()->name;

            $user->save();

            return redirect()->route('blackbelt-list');
        }else{
            return null;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlackBest  $blackBest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $black_belt_user = BlackBelt::findorfail($request->id);
        $black_belt_user->delete();
        $result = [
            'response' => 'success',
            'code' => '200'
        ];

        return response()->json($result);
    }

    public function recovery($id){
        $blackbelt = BlackBelt::withTrashed()->findorfail($id);
        if($blackbelt->deleted_at != null){
            $blackbelt->deleted_at = null;
            $blackbelt->save();
            return redirect()->route('view-blackbelt',['id'=>$id]);
        }
        else
            return redirect()->route('blackbelt-list');
    }


    public function page_view(){
        $values = BlackBelt::all();
        return view('page.instructor',compact('values'));
    }

    public function index_page(){
        $values = BlackBelt::where('instructor','yes')
            ->get();
        return view('page.index',compact('values'));
    }


    /**
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile_view($name){
        $user = BlackBelt::where('full_name',$name)->get();
        $value =  $user[0];
        if($value->instructor == 'yes'){
            $instructor = Instructor::where('black_belt_id',$value->id)->get();
            $teacher = $instructor[0];
            if($teacher->main_instructor == 'no')
                return view('page.profile',compact('value','teacher'));
            else
                abort(404);
        }else
            abort(404);
    }
}
