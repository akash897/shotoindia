<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{

    use SoftDeletes;

    public function image()
    {
        return $this->hasMany('App\Image');
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
