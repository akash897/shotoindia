<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlackBeltController@index_page')->name('page.index');

Route::get('/chief-instructor', function () {
    return view('page.chief-instructor');
})->name('page.chief-instructor');

Route::get('/instructor','BlackBeltController@page_view')->name('page.instructor');
Route::get('/instructor/{name}','BlackBeltController@profile_view')->name('profile.instructor');

//Auth::routes();
ROute::get('/contact-us',function (){
    return view('page.contact-us');
})->name('page.contact_us');

Route::get('/dojo',function(){
    return view('page.dojo');
})->name('page.dojo');

Route::get('/kobudo',function(){
    return view('page.kobudo');
})->name('page.kobudo');

Route::get('/branches',function(){
    return view('page.branches');
})->name('page.branches');

Route::get('/events-and-news',function(){
    return view('page.event');
})->name('page.events');

Route::get('/gallery-images','EventController@index')->name('page.event');
Route::get('/gallery-images/{event}','AlbumController@index')->name('page.album');
Route::get('/gallery-images/{event}/{album}','AlbumController@show')->name('page.image');

Route::get('/camp-2015',function(){
    return view('page.camp-2015');
})->name('page.camp.2015');

Route::get('/camp-2016',function(){
    return view('page.camp-2016');
})->name('page.camp.2016');

Route::get('/camp-2017',function(){
    return view('page.camp-2017');
})->name('page.camp.2017');

Route::get('/videos',function(){
    return view('page.video');
})->name('page.video');

Route::get('/membership',function(){
    return view('page.membership');
})->name('page.membership');
