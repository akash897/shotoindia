var infoWindow,pos,json_value;
function initMap() {

    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var labelIndex = 0;

    var hq = {lat: 22.343610, lng: 73.196796};
    // var sainath = {lat: 22.256482, lng: 73.205126};
    // var kaizen = {lat: 22.355613, lng: 73.203052};
    // var narayan = {lat: 22.282390, lng: 73.234575};
    // var bhs = {lat: 22.270985, lng: 73.212925};
    // var sainath_square = {lat:22.257815, lng:73.198857};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: hq
    });
    var head_quater = new google.maps.Marker({
        animation: google.maps.Animation.DROP,
        position: hq,
        map: map,
        title: 'Head Quarter'
    });

    // var sainath_gym = new google.maps.Marker({
    //     animation: google.maps.Animation.DROP,
    //     position: sainath,
    //     map: map,
    //     label: labels[labelIndex++ % labels.length],
    //     title: 'Jai Sainath Gymnasium'
    // });

    // var kaizen_school = new google.maps.Marker({
    //     animation: google.maps.Animation.DROP,
    //     position: kaizen,
    //     map: map,
    //     label: labels[labelIndex++ % labels.length],
    //     title: 'Kaizen School'
    // });

    // var narayan_international_school = new google.maps.Marker({
    //     animation: google.maps.Animation.DROP,
    //     position: narayan,
    //     map: map,
    //     label: labels[labelIndex++ % labels.length],
    //     title: 'Narayan International School'
    // });

    // var baroda_high_school = new google.maps.Marker({
    //     animation: google.maps.Animation.DROP,
    //     position: bhs,
    //     map: map,
    //     label: labels[labelIndex++ % labels.length],
    //     title: 'Baroda High School'
    // });

    // var sainath_square_gymnasium = new google.maps.Marker({
    //     animation: google.maps.Animation.DROP,
    //     position: sainath_square,
    //     map: map,
    //     label: labels[labelIndex++ % labels.length],
    //     title: 'SAINATH SQUARE GYMNASIUM'
    // });



    infoWindow = new google.maps.InfoWindow;
    //USER CURRENT LOCATION
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var myLocation = new google.maps.Marker({
                icon: 'https://www.robotwoods.com/dev/misc/bluecircle.png',
                position: pos,
                map: map,
            });
            map.setCenter(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }


}