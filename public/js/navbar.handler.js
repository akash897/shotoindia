$(document).ready(function(){
    var url = $("meta[name='_url']").attr('content');
    if(url.indexOf('contact-us') >= 0 ){
        $('#navbar_contact').addClass('active');
    }

    if(url.indexOf('chief-instructor') >= 0 ){
        $('#navbar_about_us').addClass('active');
    }

    if(url.indexOf('dojo') >= 0 ){
        $('#dojo').addClass('active');
    }

    if(url == 'http://localhost:8000' || url== 'http://www.shotoindia.com' ){
        $('#navbar_index').addClass('active');
    }
});