<div class="footer">
    <center>
        <div class="f-bg-w3l">
            <div class="col-md-4 w3layouts_footer_grid">
                <h2>Address</span></h2>
                <p>B-12, Navyug Soc,<br> Near Ayappa Temple ,<br> New Sama Road Vadodara <br> 390008</p>


            </div>
            <div class="col-md-4 w3layouts_footer_grid">
                <h3>Contact No</h3>
                <p><span class="fa fa-phone"></span>&nbsp;9998794592</p>

            </div>
            <div class="col-md-4 w3layouts_footer_grid">
                <h2>Email Address</h2>
                <p><span class="fa fa-envelope-o"></span>&nbsp;yogendra_budo@yahoo.com</p>

                <ul class="social_agileinfo">
                    <li><a href="https://www.facebook.com/Shotokan-Karate-do-Federation-India-128656060566632/" target="_blank" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                </ul>
            </div>


            <div class="clearfix"> </div>

        </div>
    </center>
    {{--<p class="copyright">© {{date('Y')}} Shotokan Karate-Do Fedration</p>--}}
</div>