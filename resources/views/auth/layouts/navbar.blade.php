<div class="agileits_w3layouts_banner_nav">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a class="navbar-brand" href="{{route('page.index')}}">Shotokan</a></h1>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <nav class="link-effect-2" id="link-effect-2">
                    <ul class="nav navbar-nav">
                        <li class="" id="navbar_index"><a href="{{route('page.index')}}" class="effect-3">Home</a></li>
                        <li class="dropdown" id="navbar_about_us">
                            <a href="#" class="dropdown-toggle effect-3" data-toggle="dropdown">About Us<b class="caret"></b></a>
                            <ul class="dropdown-menu agile_short_dropdown">
                                <li><a href="{{route('page.chief-instructor')}}">Chief Instructor</a></li>
                                <li><a href="{{route('page.instructor')}}">Instructors</a></li>
                            </ul>
                        </li>
                        <li class="" id="dojo"><a href="{{route('page.dojo')}}" class="effect-3">Dojo</a></li>
                        <li><a href="{{route('page.kobudo')}}" class="effect-3">Kobudo</a></li>
                        <li><a href="{{route('page.branches')}}" class="effect-3">Branches</a></li>
                        <li><a href="{{route('page.events')}}" class="effect-3">Events & News</a></li>
                        <li><a href="{{route('page.membership')}}" class="effect-3">Membership</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle effect-3" data-toggle="dropdown">Gallery<b class="caret"></b></a>
                            <ul class="dropdown-menu agile_short_dropdown">
                                <li><a href="{{route('page.event')}}">Image Gallery</a></li>
                                <li><a href="{{route('page.video')}}">Video Gallery</a></li>
                            </ul>
                        </li>
                        <li class="" id="navbar_contact"><a href="{{route('page.contact_us')}}" class="effect-3">Contact Us</a></li>
                        <li class="" id="navbar_contact"><a href="https://admin.shotoindia.com/login" target="_blank" class="effect-3">Login</a></li>
                    </ul>
                </nav>
            </div>
        </nav>
        <div class="clearfix"> </div>
    </div>
</div>