<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Shotokan Karate do Federation India</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Shotokan Karate do Federation India"/>
    <meta name="description" content=""/>
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <!-- Owl-carousel-CSS -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <!-- font-awesome-icons -->
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
</head>
<style>

    .round-image {
        border-radius: 5%;
    }

    .instructor-icon {
        height: 200px;
        width: 200px;
    }

    .blackbelt-icon{
        height : 200px;
        width : 200px;
    }
</style>
<body>
<!-- banner -->
<div class="main_section_agile inner">
    @include('auth.layouts.navbar')
</div>
<!-- //banner -->
<!-- about inner -->
<div class="about-bottom inner-padding">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">Instructors</h3>
        </div>
        <div class="w3l-info">
            @foreach($values as $value)
                @if($value->instructor=='yes' && $value->instructor_detail->main_instructor == 'no')
                    <a href="{{route('profile.instructor',['name'=>$value->full_name])}}">
                        <div class="col-md-4 ser-lef-agile" data-aos="flip-down">
                            <div class="grid1">
                                <img src="{{ !empty($value->image_url) ? env('ADMIN_URL').'/'.$value->image_url : 'images/black-belt/user-icon.png' }}" class="round-image instructor-icon" alt=""/>
                                <h4>{{ $value->full_name }}</h4>
                                <h6>{{ $value->dan_list->dan }}</h6>
                                <h5>{{ !empty($value->instructor_detail->branch) ? $value->instructor_detail->branch : '' }}</h5>
                            </div>
                        </div>
                    </a>
                @endif
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div class="team">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">Black Belts</h3>
        </div>
        <div class="w3l-info">
            @foreach($values as $value)
                @if($value->instructor=='no')
                    <div class="col-md-4 ser-lef-agile" data-aos="flip-down">
                        <div class="grid1">
                            <img src="{{ !empty($value->image_url) ? env('ADMIN_URL').'/'.$value->image_url : 'images/black-belt/user-icon.png' }}" class="round-image blackbelt-icon" alt=""/>
                            <h4>{{ $value->full_name }}</h4>
                            <h6>{{ $value->dan_list->dan }}</h6>
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //Team -->
<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
<!-- //js -->
<script type="text/javascript" src="{{ asset('js/bootstrap-3.1.1.min.js') }}"></script>
<!-- for testimonials slider-js-script-->


<script src="{{asset('js/navbar.handler.js')}}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
    AOS.init();
</script>
</body>

</html>