<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Contact Us</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Shotokan Karate do Federation India,Contact US" />
    <meta name="description" content=""  />
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link rel="stylesheet" href="css/lightbox.css"><!-- For-gallery-CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Owl-carousel-CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
    <style>
        #map {
            height: auto;
            width: 100%;
            min-height: 500px;
        }
    </style>
</head>

<body>
<!-- banner -->
<div class="main_section_agile inner">
    @include('auth.layouts.navbar')
</div>
<!-- //banner -->
<!--gallery-->
<div class="gallery">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header"><span class="fa fa-map-marker"></span> Google Map</h3>
        </div>
        <div class="gallery-info">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14761.189548454562!2d73.1968186!3d22.3423972!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe9462b9bb567cc02!2sShotokan%20Karate-Do%20Fedration%20India!5e0!3m2!1sen!2sin!4v1572869928066!5m2!1sen!2sin" frameborder="0" style="border:0; hight: auto; width: 100%; min-height: 510px;" allowfullscreen=""></iframe>
{{--            <div id="map"></div>--}}
{{--            <div class="clearfix"> </div>--}}
        </div>
{{--        <script src="{{asset('js/contact-us.google-map.js')}}"></script>--}}
    </div>
</div>
<!--//gallery-->
<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- for testimonials slider-js-script-->

{{--<script src="https://www.google.com/maps/embed/v1/MODE?key=AIzaSyCLGTv1zttyegOAcRdKJkhEj1KOQYE6G8Y"></script>--}}
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLGTv1zttyegOAcRdKJkhEj1KOQYE6G8Y&callback=initMap" sync defer></script>--}}

<script src="{{asset('js/navbar.handler.js')}}"></script>

</body>

</html>