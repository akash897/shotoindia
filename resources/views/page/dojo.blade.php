<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Shotokan Karate do Federation India</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Shotokan Karate do Federation India"/>
    <meta name="description" content=""/>
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Owl-carousel-CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
</head>
<style>

    .round-image {
        border-radius: 50%;
    }

    .profile-icon {
        height: 200px;
        width: 200px;
    }
</style>
<body>
<!-- banner -->
<div class="main_section_agile inner">
    @include('auth.layouts.navbar') {{--// NAV BAR--}}
</div>
<!-- //banner -->
<!-- about inner -->
<div class="about-bottom inner-padding">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">Dojo</h3>
        </div>
        <h3 class="heading-agileinfo white-w3ls">Hombu Dojo at Baroda.</h3>
        <div class="about-bott-right">
            {{--<h5>Who We Are</h5>--}}
            <br><br><img src="images/dojo.jpg" class="img-responsive col-md-4 col-sm-6">
            <p class="">The 1st Martial Arts Dojo (Gym) in Gujarat, India.</p>
            <br><p>

                “Sharma’s Academy of Martial Arts” is the First Martial Arts Dojo (Gym) in Gujarat. It is also the Hombu Dojo of Shotokan Karatedo Federation India. This Gym is formed by Sensei Yogendra Sharma with an experience of 32 Years Studying & Teaching Martial Arts..
            </p>
            <br>
            <p>
                Sharma’s Academy takes pride in providing a family, friendly atmosphere, offering classes for Adults, teens & Children. Everyone helps and supports each other & new members are always made to feel welcome.
            </p>
            <br>
            <p>
                The most important goal of the Academy is strengthen people Physically & Mentally through strong disciplined training, So that better harmony among people is realized.
            </p>



        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- for testimonials slider-js-script-->


<script src="{{asset('js/navbar.handler.js')}}"></script>
</body>

</html>