<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Shotokan Karate do Federation India</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Shotokan Karate do Federation India"/>
    <meta name="description" content=""/>
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!-- Owl-carousel-CSS -->

    <!-- Testimonials-slider-css-files -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}" type="text/css" media="all">
    <link href="{{asset('css/owl.theme.css')}}" rel="stylesheet">
    <!-- //Testimonials-slider-css-files -->

    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <!-- font-awesome-icons -->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
</head>
<style>
    .w3l_header {
        color: red;
    }
    .round-image{
        border-radius: 50%;
    }
    .profile-icon{
        height: 200px;
        width: 200px;
    }
</style>
<body>
<!-- banner -->
<div class="main_section_agile" id="home">
    @include('auth.layouts.navbar')
</div>

<div class="banner_top">
    <div class="slider">
        <div class="wrapper">

            <!-- Slideshow 3 -->
            <ul class="rslides" id="slider">
                <li>
                    <div class="agile_banner_text_info"></div>
                </li>
                <li>
                    <div class="agile_banner_text_info"></div>
                </li>
                <li>
                    <div class="agile_banner_text_info"></div>
                </li>
                <li>
                    <div class="agile_banner_text_info"></div>
                </li>
                <li>
                    <div class="agile_banner_text_info"></div>
                </li>
                <li>
                    <div class="agile_banner_text_info"></div>
                </li>
                {{--<li>--}}
                    {{--<div class="agile_banner_text_info"></div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="agile_banner_text_info"></div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="agile_banner_text_info"></div>--}}
                {{--</li>--}}
            </ul>
            <!-- Slideshow 3 Pager -->
        </div>
    </div>
</div>
<!-- //banner -->
<!-- About us -->
<div class="about-3">
    <div class="wthree_head_section">
        <h1 class="w3l_header">Shotokan Karate-Do Fedration India</h1>
    </div>
    <div class="container">
        <h4>Shotokan Karatedo Federation India is approved by Karata Association of India (Governing Body of Karate in
            India). Sensei Yogendra Sharma is the Chief Instructor and Technical Director of S.K.F. India.</h4>
        <br>
        <h4>He is learning and training Karate since 1980. The Headquarter of S.K.F. India is situated in
            Baroda, Gujarat, India. It discover training in Traditional Shotokan Karate, Okinawan Kobudo & Self
            Defence.</h4>
        <br>
        <div class="pull-right">
            <button class="btn btn-default">Read More ..</button>
        </div>
    </div>
</div>
<!-- //About us -->
<div class="ser-agile">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">Our Instructors</h3>
        </div>
        <div class="w3l-info">
            @foreach($values as $value)
                @if($value->instructor=='yes' && $value->instructor_detail->main_instructor == 'yes')
                    <div class="col-md-4 ser-lef-agile">
                        <div class="grid1">
                            <img src="{{ !empty($value->image_url) ? env('ADMIN_URL').'/'.$value->image_url : 'images/black-belt/user-icon.png' }}" class="round-image profile-icon" alt=""/>
                            <h4>{{ $value->full_name }}</h4>
                            <h6>{{ $value->dan_list->dan }}</h6>
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- testimonials -->

<!-- testimonals section -->
<div class="testimonals">
    <div class="container">
        <div class="container">
            <div class="wthree_head_section">
                <h3 class="w3l_header w3_agileits_header two">About US</h3>
            </div>
            <div class="about-bott-right">
                <p>“Sharma’s Academy of Martial Arts” is the First Martial Arts Dojo (Gym) in Gujarat. It is also the Hombu
                Dojo of Shotokan Karatedo Federation India. This Gym is formed by Sensei Yogendra Sharma with an
                    experience of 32 Years of studying & teaching Martial Arts.</p>

                <p>Sharma’s Academy takes pride in providing a family friendly atmosphere, offering classes for adults,
                    teens & children. Everyone helps and supports each other & new members are always made to feel welcome.</p>

                <p>The most important goal of the Academy is to strengthen people physically & mentally through strong
                    disciplined training, so that better harmony among people is realized.</p>
            </div>
            <div class="pull-right">
                <button class="btn btn-default">Read More ..</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- /testimonals section -->

<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- Slider script -->
<script src="js/responsiveslides.min.js"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            nav: true,
            manualControls: '#slider3-pager',
        });
    });
</script>
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>

<!-- for testimonials slider-js-file-->
<script src="js/owl.carousel.js"></script>
<!-- //for testimonials slider-js-file-->
<script>
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({

            autoPlay: 3000, //Set AutoPlay to 3 seconds
            autoPlay: true,
            items: 3,
            itemsDesktop: [640, 5],
            itemsDesktopSmall: [414, 4]
        });
    });
</script>
<!-- for testimonials slider-js-script-->
<!-- stats -->
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.countup.js"></script>
<script>
    $('.counter').countUp();
</script>
<!-- //stats -->

<script src="{{asset('js/navbar.handler.js')}}"></script>

</body>

</html>