<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Shotokan Karate do Federation India</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Shotokan Karate do Federation India"/>
    <meta name="description" content=""/>
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Owl-carousel-CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">

</head>
<style>

    .round-image {
        border-radius: 50%;
    }

    .profile-icon {
        height: 200px;
        width: 200px;
    }

    .img-thumbnail{
        background: transparent;
        border: none;
    }

    .img-thumbnail:hover {
        transform: scale(1.2);
    }

    h4{
        color: #fff;
    }
</style>
<body>
<!-- banner -->
<div class="main_section_agile inner">
    @include('auth.layouts.navbar')
</div>
<!-- //banner -->
<!-- about inner -->
<div class="about-bottom inner-padding">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">The Organisation that Teaches Traditional Shotokan Karate</h3>
        </div>
        <div class="about-bott-right">
            <h3 class="heading-agileinfo text-center white-w3ls">Gasshuku</h3>
            <div class="container">
                <div class="gallery">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <a href="images/event/camp2017/1.jpg" data-lightbox="Class Room">
                                <img src="images/event/camp2017/thumbnails/1_tn.jpg" style="width: 100%;" class="img-thumbnail"/>
                                <h4 class="heading-agileinfo text-center white-w3ls" style="font: bold;">2017</h4>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <a href="images/event/camp2016/25.jpg" data-lightbox="Class Room">
                                <img src="images/event/camp2016/thumbnails/25_tn.jpg" style="width: 100%;" class="img-thumbnail"/>
                                <h4 class="heading-agileinfo text-center white-w3ls">2016</h4>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <a href="images/event/camp2015/1.jpg" data-lightbox="Class Room">
                                <img src="images/event/camp2015/thumbnails/1_tn.jpg" style="width: 100%;" class="img-thumbnail"/>
                                <h4 class="heading-agileinfo text-center white-w3ls">2015</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>

            <br><br>


            <h3 class="heading-agileinfo text-center white-w3ls">SKF INDIA Gujarat Karate & Kobudo Championship
                2013</h3><br>
            <br><br>
            <span class="col-lg-4 col-md-4 col-sm-6">
                <a href="images/events/champion2013/1.jpg" data-lightbox="Class Room">
                    <img src="images/events/champion2013/1.jpg" style="width: 100%;" class="img-thumbnail"/>
                </a>
            </span>
            <p class="">Recently Shotokan Karate Federation India organised SKF India Gujarat Karate & Kobudo
                Championship 2013 on 18th March 2012 at O.N.G.C. Officers’ Club, Makarpura Road, Baroda, Gujarat. Ageing
                about 5 to 55 Years around more than 150 Participants were in the tournament. Sensei Yogendra Sharma,
                Sensei Ajay Patel & Sensei Prashant Rajput were in the Main Referee Panel.</p>

            <div class="clearfix"></div>
            <br><br><br>

            <h3 class="heading-agileinfo text-center white-w3ls">GASSHUKU 2012</h3><br>
            <br><br>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <a href="images/events/gasshuku/1.jpg" data-lightbox="Class Room">
                    <img src="images/events/gasshuku/1.jpg" style="width: 100%;" class="img-thumbnail"/>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <a href="images/events/gasshuku/2.jpg" data-lightbox="Class Room">
                    <img src="images/events/gasshuku/2.jpg" style="width: 100%;" class="img-thumbnail"/>
                </a>
            </div>
            <div class="clearfix"></div>
            <br>
            <p class="">Gasshuku was organised by Sensei Yogendra Sharma (Chief Instructor, Shotokan Karate Federation
                India), The camp was assisted by Sensei Ajay Patel & Sensei Prashant Rajput. In which from All over
                Gujarat Students as well as Senior Black Belts Instructors has participated. The camp was of 5 days in
                which an Intensive Training of Karate and Kobudo was given to the Students.</p>
            <br>
            <p>
                The Camp was organised with its aim to push your limits of Budokas as the word push your limit is given
                by Sensei Yogendra Sharma ; it means to increase the ability and stamina to practice Martial Arts. The
                Students has learned several Drills of Increasing Stamina, Practiced Basics of Karate & Kobudo, New
                Fighting Techniques, hard Conditioning for Self Defence, Advanced Kumite Techniques especially based on
                Heian Katas, Advanced Karate Asai Ha Kata Roshu, Kobudo Bo Kata Shushi No Kon, Nunchaku Kata Kobu
                Nunchaku & its Application, Fitness training basic as well as other Katas also Several techniques of
                Self Defence to protect our self and others in todays world.
            </p>
            <br>
            <p>
                The camp was organised at Beach where students have enjoyed the healthy environment with training. On
                the last day there was also a camp fire. In conclusion and closing ceremony certificates to all students
                has been awarded
            </p>
            <br><br><br>


            <h3 class="heading-agileinfo text-center white-w3ls">SKF INDIA Gujarat Karate & Kobudo Championship
                2012</h3><br>
            <p class="">Recently Shotokan Karate Federation India organised SKF India Gujarat Karate & Kobudo
                Championship 2012 on 18th March 2012 at Nizampura Atithi Gruh, Baroda, Gujarat. Ageing about 5 to 55
                Years around more than 130 Participants were in the tournament. Sensei Yogendra Sharma, Sensei Ajay
                Patel & Sensei Prashant Rajput were in the Main Referee Panel</p>
            <br>
            <br><br><br>

            <h3 class="heading-agileinfo text-center white-w3ls">GASSHUKU 2011</h3>
            <div class="gallery">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <a href="images/events/gasshuku2011/1.jpg" data-lightbox="Class Room">
                            <img src="images/events/gasshuku2011/1.jpg" style="width: 100%;" class="img-thumbnail"/>
                        </a>
                    </div>
                </div>
            </div>

            <br><br>


            <h3 class="heading-agileinfo text-center white-w3ls">Okinawan Kobudo Training Camp 2011</h3><br>
            <br><br>
            <span class="col-lg-4 col-md-4 col-sm-6">
                <a href="images/events/2014/1.jpg" data-lightbox="Class Room">
                    <img src="images/events/champion2013/1.jpg" style="width: 100%;" class="img-thumbnail"/>
                </a>
            </span>
            <p class="">The Okinawan Kobudo Training Camp 2011 was organised at Sharma’s Academy of Martial Arts ( SKF
                India Hombu dojo) in the month of May 2011. In the Camp the students were taught the weapons Bo and
                Nunchaku. There were more than 15 Students who took active part in the 15 Days Kobudo Camp.</p>

            <div class="clearfix"></div>
            <br><br><br>


            <h3 class="heading-agileinfo text-center white-w3ls">Asai Memorial Training Camp & Black Belt Awarding
                2010</h3><br>
            <br><br>
            <span class="col-lg-4 col-md-4 col-sm-6">
                <a href="images/events/blackbeltaward/1.jpg" data-lightbox="Class Room">
                    <img src="images/events/blackbeltaward/1.jpg" style="width: 100%;" class="img-thumbnail"/>
                </a>
            </span>
            <p class="">The Asai Memorial Training Camp and Black Belt Awarding was organised at ONGC Officer’s Club on
                15th August (Death Anniversary of The Great Karate Master Late Shihan Tetsuhiko Asai) 2010 with the aim
                to Spread the Teachings of Asai Sensei.</p>

            <div class="clearfix"></div>
            <br><br><br>


            <h3 class="heading-agileinfo text-center white-w3ls">1st Akshay Kumar Invitational Karate Championship
                2009</h3><br>
            <br><br>
            <span class="col-lg-4 col-md-4 col-sm-6">
                <a href="images/events/akshaykumar/1.jpg" data-lightbox="Class Room">
                    <img src="images/events/akshaykumar/1.jpg" style="width: 100%;" class="img-thumbnail"/>
                </a>
            </span>

            <div class="clearfix"></div>
            <br><br><br>

            <h3 class="heading-agileinfo text-center white-w3ls">Martial Arts Seminar for Security Guards</h3><br>
            <br><br>
            <span class="col-lg-4 col-md-4 col-sm-6">
                <a href="images/events/security/1.jpg" data-lightbox="Class Room">
                    <img src="images/events/security/1.jpg" style="width: 100%;" class="img-thumbnail"/>
                </a>
            </span>
            <p class="">Martial Arts Seminar for Security Guards Organised by SS Boss Security Services at Ankleshwar
                GIDC. The Seminar was conduced by Sensei Yogendra Sharma (Chief Instructor, Shotokan Karatedo Federation
                India). During the Seminar Omprakash Singh (M.D, SS Boss Security) who himself is a Black Belt in Karate
                has assisted Sensei Sharma During the Seminar.</p>

            <p class="">In the Seminar the Guards were taught the Special Tactics of Martial Arts to overcome
                threatening Situations. In the Seminar unarmed Combat and Use of Security Staff , Defence against
                gribbing Techniques were taught. Around More than 25 Guards took part in the Seminar from Various
                Branches of SS Boss Security Services. On the Completition of the Course Certificate were Awarded to
                Guards by Srimati Sushila Devi Singh.</p>

            <div class="clearfix"></div>
            <br><br><br>


        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- for testimonials slider-js-script-->

<link rel="stylesheet" href="css/lightbox.min.css"/>
<script src="js/lightbox.min.js"></script>

<script src="{{asset('js/navbar.handler.js')}}"></script>
</body>

</html>