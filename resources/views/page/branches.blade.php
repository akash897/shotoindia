<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Shotokan Karate do Federation India</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Shotokan Karate do Federation India"/>
    <meta name="description" content=""/>
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Owl-carousel-CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
</head>
<style type="text/css">
    .grid1 {
        height: 250px;
    }


    .overlay {
        margin-left: 15px;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: #666;
        opacity: 0.7;
        overflow: hidden;
        width: 94.736842105%;
        height: 0;
        transition: .5s ease;
    }

    .ser-lef-agile:hover .overlay {
        height: 250px;
    }
</style>
<body>
<!-- banner -->
<div class="main_section_agile inner">
    @include('auth.layouts.navbar')
</div>
<!-- //banner -->
<!-- about inner -->
<div class="about-bottom inner-padding">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">Branches</h3>
        </div>
        <div class="w3l-info">
            <div class="col-md-6 ser-lef-agile no-padding">
                <div class="grid1">
                    <h4>Head Quarter</h4>
                    <h6>INSTRUCTOR - YOGENDRA SHARMA (M:9998794592)</h6>
                    <h5>B – 12, Navyug Society, New Sama Road, Vadodara – 390008, Gujarat.</h5>
                </div>
                {{--<div class="overlay">--}}
                    {{--akash--}}
                {{--</div>--}}
            </div>
            <div class="col-md-6 ser-lef-agile no-padding">
                <div class="grid1">
                    <h4>My Gym</h4>
                    <h6>INSTRUCTOR - YOGENDRA SHARMA (M:9998794592)</h6>
                    <h5>1st Floor, Nityanand Complex, Sussen Tarsali Ring Road, Tarsali, Makarpura, Vadodara.</h5>
                </div>
                {{--<div class="overlay">--}}
                    {{--akash--}}
                {{--</div>--}}
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>New Sarwa Mangal(Kizen) School</h4>
                    <h6>INSTRUCTOR - SAMIR SHAIKH (M:8347805204)</h6>
                    <h5>Sama Savli Road, Vadodara, Gujarat</h5>
                </div>
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>Shir Narayan International School</h4>
                    <h6>INSTRUCTOR - SAMIR SHAIKH (M:8347805204)</h6>
                    <h5>Waghodia-Dabhoi Ring Road, Vadodara, Gujarat</h5>
                </div>
                {{--<div class="overlay">--}}
                    {{--akash--}}
                {{--</div>--}}
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>Shree Vidya Learning Center</h4>
                    <h6>INSTRUCTOR - SAMIR SHAIKH (M:8347805204)</h6>
                    <h5>Maneja, Vadodara, Gujarat</h5>
                </div>
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>Baroda High School</h4>
                    <h6>INSTRUCTOR - SAMIR SHAIKH (M:8347805204)</h6>
                    <h5>Danteshwar, Vadodara, Gujarat</h5>
                </div>
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>Sanskar Bhoomi School</h4>
                    <h6>INSTRUCTOR - SAMIR SHAIKH (M:8347805204)</h6>
                    <h5>Vadu, Vadodara, Gujarat</h5>
                </div>
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>Vrundavan School</h4>
                    <h6>INSTRUCTOR - SAMIR SHAIKH (M:8347805204)</h6>
                    <h5>Makarpura, Vadodara, Gujarat</h5>
                </div>
                {{--<div class="overlay">--}}
                    {{--akash--}}
                {{--</div>--}}
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>Royal Arces Club</h4>
                    <h6>INSTRUCTOR - SAMIR SHAIKH (M:8347805204)</h6>
                    <h5>Vadsar Road, Vadodara, Gujarat</h5>
                </div>
                {{--<div class="overlay">--}}
                    {{--akash--}}
                {{--</div>--}}
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>Kosamba Branch</h4>
                    <h6>Mr. Omprakash Singh (M:09879308533)</h6>
                    <h5>Kosamba, Dist. Bharuch</h5>
                </div>
            </div>
            <div class="col-md-6 ser-lef-agile">
                <div class="grid1">
                    <h4>G.I.D.C. Branch</h4>
                    <h6>INSTRUCTOR - DARPAN RAVAL (M:81286 53014)</h6>
                    <h5>D-19, Gajanan Park Soc, Vadsar Road, G.I.D.C, Makarpura, Vadodara</h5>
                </div>
                {{--<div class="overlay">--}}
                    {{--akash--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- for testimonials slider-js-script-->


<script src="{{asset('js/navbar.handler.js')}}"></script>
</body>

</html>