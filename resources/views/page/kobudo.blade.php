<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Shotokan Karate do Federation India</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Shotokan Karate do Federation India"/>
    <meta name="description" content=""/>
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Owl-carousel-CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
</head>
<style>

    .well{
        background-color: #5e5d5d;
         border: 0px solid #e3e3e3;
        margin: 5px;
    }

    li > a {
        color : white;
    }

    .nav > li > a:hover, .nav > li > a:focus {
        background: #777;
        color: white;
        text-decoration: none;
    }

    .nav > li > a:active {
        background: #777;
        color: white;
        text-decoration: none;
    }

    .text-pills{
        font-size: 14px;
        letter-spacing: 1px;
        margin: 0;
        padding-top: 10px;
        color: #fff;
        line-height: 1.8em;
    }
</style>
<body>
<!-- banner -->
<div class="main_section_agile inner">
    @include('auth.layouts.navbar')
</div>
<!-- //banner -->
<!-- about inner -->
<div class="about-bottom inner-padding">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">Kobudo</h3>
        </div>
        <div class="col-md-3 well">
            <ul class="nav">
                <li class="active"><a data-toggle="pill" href="#nanchaku">Nunchaku</a></li>
                <li><a data-toggle="pill" href="#tonfa">Tonfa</a></li>
                <li><a data-toggle="pill" href="#kama">Kama</a></li>
                <li><a data-toggle="pill" href="#eku">Eku</a></li>
                <li><a data-toggle="pill" href="#sai">Sai</a></li>
                <li><a data-toggle="pill" href="#bo">Bo</a></li>
            </ul>
        </div>

        <div class="tab-content col-md-8 well">
            <div id="nanchaku" class="tab-pane fade in active">
                <h3>Nanchaku</h3>
                <br><img src="images/kobudo/nanchaku.jpg" class="img-responsive col-md-4 no-padding col-sm-6">
                <span class="text-pills">A nunchaku is two sections of wood (or metal in modern incarnations) connected by a cord or chain. There is much controversy over its origins: some say it was originally a Chinese weapon, others say it evolved from a threshing flail, while one theory purports that it was developed from a horse’s bit. Chinese nunchaku tend to be rounded, whereas Japanese are octagonal, and they were originally linked by horse hair. There are many variations on the nunchaku, ranging from the three sectional staff (san-setsu-kon nunchaku, mentioned later in this article), to smaller multi-section nunchaku. The nunchaku was popularized by Bruce Lee in a number of films, made in both Hollywood and Hong Kong. Now it is also made with chains or rope in between.</span>
            </div>
            <div id="tonfa" class="tab-pane fade">
                <h3>Tonfa</h3>
                <br><img src="images/kobudo/tonfa.jpg" class="img-responsive col-md-4 no-padding col-sm-6">
                <span class="text-pills">The tonfa is more readily recognized by its modern development in the form of the police nightstick, although its usage differs. It supposedly originated as the handle of a millstone used for grinding grain. The tonfa is traditionally made from red oak, and can be gripped by the short perpendicular handle or by the longer main shaft. As with all Okinawan weapons, many of the forms are reflective of “empty hand” techniques.</span>
            </div>
            <div id="kama" class="tab-pane fade">
                <h3>Kama</h3>
                <br><img src="images/kobudo/kama.jpg" class="img-responsive col-md-4 no-padding col-sm-6">
                <span class="text-pills">The kama is the traditional farming sickle, and considered one of the hardest to learn due to the inherent danger in practicing with such a weapon. The point at which the blade and handle join in the “weapon” model normally has a nook with which a bo can be trapped, although this joint proved to be a weak point in the design, and modern day examples tend to have a shorter handle with a blade that begins following the line of the handle and then bends, though to a lesser degree; this form of the kama is known as the natagama. The edge of a traditional rice sickle, such as one would purchase from a Japanese hardware store, continues to the handle without a notch, as this is unneeded for its intended use.</span>
            </div>
            <div id="eku" class="tab-pane fade">
                <h3>Eku</h3>
                <br><img src="images/kobudo/eku.jpg" class="img-responsive col-md-4 no-padding col-sm-6">
                <span class="text-pills">The Okinawan style of oar is called an eku (this actually refers to the local wood most commonly used for oars), eiku, iyeku, or ieku. Noteworthy hallmarks are the slight point at the tip, curve to one side of the paddle and a roof-like ridge along the other. One of the hojoundo (basic moves) for this weapon utilizes the fact that a fisherman fighting on the beach would be able to fling sand at an opponent. While not having the length, and therefore reach, of the bo, the rather sharp edges can inflict more penetrating damage when wielded properly.</span>
            </div>
            <div id="sai" class="tab-pane fade">
                <h3>Sai</h3>
                <br><img src="images/kobudo/sai.jpg" class="img-responsive col-md-4 no-padding col-sm-6">
                <span class="text-pills">The sai is sometimes mistakenly believed to be a variation on a tool used to create furrows in the ground, however this is highly unlikely as metal on Okinawa was in short supply at this time and a stick would have served this purpose more satisfactorily for a poor commoner, or Heimin. The sai appears similar to a short sword, but is not bladed and the end is traditionally blunt. Records from China prove its original existence although in a much more elongated form where it was known as Tsai and was used purely as a weapon. The weapon is metal and of the truncheon class with its length dependent upon the forearm of the user. The two shorter prongs on either side of the main shaft are used for trapping (and sometimes breaking) other weapons such as a sword or bo. The sai originally reached Japan in the form of the jitte or jutte, which has only a single prong. Both are truncheon-like weapons, used for striking and bludgeoning. </span>
            </div>
            <div id="bo" class="tab-pane fade">
                <h3>Bo</h3>
                <br><img src="images/kobudo/bo.jpg" class="img-responsive col-md-4 no-padding col-sm-6">
                <span class="text-pills">The bō is a six-foot staff, sometimes tapered at either end. It was perhaps developed from a farming tool called a tenbin: a stick placed across the shoulders with baskets or sacks hanging from either end. The bo was also possibly used as the handle to a rake or a shovel. The bo, along with shorter variations such as the jo and hanbo could also have been developed from walking sticks used by travelers, especially monks. The bo is considered the ‘king’ of the Okinawa weapons, as all others exploit its weaknesses in fighting it, whereas when it is fighting them it is using its strengths against them. The bo is the earliest of all Okinawa weapons (and effectively one of the earliest of all weapons in the form of a basic staff), and is traditionally made from red or white oak.</span>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- for testimonials slider-js-script-->


<script src="{{asset('js/navbar.handler.js')}}"></script>
</body>

</html>