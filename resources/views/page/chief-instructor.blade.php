<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Shotokan Karate do Federation India</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Shotokan Karate do Federation India"/>
    <meta name="description" content=""/>
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Owl-carousel-CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
</head>
<style>

    .round-image {
        border-radius: 50%;
    }

    .profile-icon {
        height: 200px;
        width: 200px;
    }
</style>
<body>
<!-- banner -->
<div class="main_section_agile inner">
    @include('auth.layouts.navbar')
</div>
<!-- //banner -->
<!-- about inner -->
<div class="about-bottom inner-padding">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">Chief Instructor</h3>
        </div>
        <div class="about-bott-right">
            {{--<h5>Who We Are</h5>--}}
            <img src="images/1192-FILEminimizer.jpg" class="img-responsive col-md-4 col-sm-6">
            <p class="">Sensei Yogendra Sharma is the Chief Instructor of SKF India and Black belt 6th Dan in Shotokan
                Karate. He
                was born on 28th November 1962 in Gujarat. He has completed his Bachelor of Commerce from M. S.
                University Baroda in the Year 1984. He had started Karate training in 1980 Under Sensei Shirish Thite
                who was affiliated with Japan Karate Association of India, Bombay Which was headed by Dr. Sharad Parekh
                Sensei.</p>
            <br>
            <p>
                He is trained in three major styles of Karate i.e. Shotokan, Goju-ryu and Shito ryu under the most
                experienced and senior masters of India as well as Japan. He is also practicing other forms of Martial
                Art i.e. Kobudo, Tai Chi & Yoga. In 1985 he had started dojos in various districts of Gujarat State.
                After many Years of training in karate he got an approach to join the Asai Sensei’s JKA Group in India
                headed by Sensei Pemba Tamang who was the Chief Instructor for India and JKA Head Quarter Instructor and
                was coming regularly to impart training in India
            </p>
            <br>
            <p>
                Sensei Sharma has Passed his 1st & 2nd Dan from Japan Karate Association of India under Sensei Pemba
                Tamang, 3rd Dan from World Shotokan Karate do Federation India, 4th Dan from All India Karatedo
                Federation and 6th from Karate Association of India in 2017. 
                Sensei Sharma has trained thousands of people incliding Security Agencies, Air Force, CRPF Jawans, N.C.C. Cadets and many other karate schools through out India.
            </p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="about-agile inner-padding">
    <div class="container">
        <h3 class="heading-agileinfo white-w3ls">Achievements</h3>
        <ul class="pull-left">
            <li><p style="text-align: left">Chief Instructor of Shotokan Karatedo Federation India (SKF India)</p></li>
            <li><p style="text-align: left">Chief Instructor and Founder of Sharma's Acedamy of Martial Arts.</p></li>
            <li><p style="text-align: left">Won Gold Medal Consecutively three years in All Gujarat Open Style Karate
                    Championship from 1988-1990</p></li>
            <li><p style="text-align: left">Won Silver Medal in team kata in JKA Shotokan India National Championship
                    held at Amritsar, Punjab in 1996.</p></li>
        </ul>
    </div>
</div>
<!-- //about inner -->
<!-- Team -->
<div class="team">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">Our Instructors</h3>
        </div>
        <div class="w3l-info">
            <div class="col-md-2"></div>
            <div class="col-md-4 ser-lef-agile">
                <div class="grid1">
                    <img src="images/pre.jpg" class="round-image profile-icon"/>
                    <h4>Prashant Rajput</h4>
                    <h6>Black Belt 4th Dan</h6>
                </div>
            </div>
            <div class="col-md-4 ser-lef-agile">
                <div class="grid1">
                    <img src="images/ajay.jpg" class="round-image profile-icon"/>
                    <h4>Ajay Patel</h4>
                    <h6>Black Belt 4th Dan</h6>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //Team -->
<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- for testimonials slider-js-script-->


<script src="{{asset('js/navbar.handler.js')}}"></script>
</body>

</html>