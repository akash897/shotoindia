<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>{{ $image_album_name }}</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Shotokan Karate, {{ $image_album_name }}"/>
    <meta name="description" content=""/>
    <meta name="_url" content="{{url()->current()}}">
    <meta name="csrf_token" content="{{csrf_token()}}">
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //custom-theme -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <!-- Owl-carousel-CSS -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>
    <!-- font-awesome-icons -->
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">

</head>
<style>

    .round-image {
        border-radius: 50%;
    }

    .profile-icon {
        height: 200px;
        width: 200px;
    }
    h4{
        color: #fff;
    }

    .img-thumbnail{
        border: 0px;
        background-color: black;
        margin: 10px 0;
    }

</style>
<body>
<!-- banner -->
<div class="main_section_agile inner">
    @include('auth.layouts.navbar')
</div>
<!-- //banner -->
<!-- about inner -->
<div class="about-bottom inner-padding">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header w3_agileits_header two">{{ $image_album_name }}</h3>
        </div>
        <div class="about-bott-right">
            {{--            <h3 class="heading-agileinfo text-center white-w3ls">2016</h3>--}}
            <div class="container">
                <div class="gallery">
                    <div class="row">
                        @foreach($images as $image)
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="{{ str_replace('public','storage',env('ADMIN_URL').'/'.$image->image_url ) }}" data-lightbox="Class Room">
                                    <img src="{{ str_replace('public','storage',env('ADMIN_URL').'/'.$image->thumbnail_url ) }}" style="align-items: center" class="img-thumbnail"/>
{{--                                    <h4 class="heading-agileinfo text-center white-w3ls" style="font: bold;">asdasdas</h4>--}}
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <br><br>




        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- footer -->
@include('auth.layouts.footer')
<!-- //footer -->
<!-- js -->
<script type="text/javascript" src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
<!-- //js -->
<script type="text/javascript" src="{{ asset('js/bootstrap-3.1.1.min.js') }}"></script>
<!-- for testimonials slider-js-script-->

<link rel="stylesheet" href="{{ asset('css/lightbox.min.css') }}"/>
<script src="{{ asset('js/lightbox.min.js') }}"></script>

<script src="{{asset('js/navbar.handler.js')}}"></script>
</body>

</html>