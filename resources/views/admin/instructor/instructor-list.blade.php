@include('admin.layouts.nav')

<div id="wrapper">
    @include('admin.layouts.side-nav')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <div id="page-wrapper" >
        <div class="container" style="width: 90%;">
            <h2>Instructor List</h2>
            <br/><br/><br/>
            <table id="example" class="table table-striped">
                <thead>
                <tr>
                    <th width="10%">Sr no.</th>
                    <th width="20%">Full Name</th>
                    <th width="20%">Rank</th>
                    <th width="20%">Update</th>
                </tr>
                </thead>
                <tbody>
                    @if(!empty($values))
                        @foreach ($values as $value)
                            <tr id="row_{{$value->id}}"class="{{ $value->deleted_at == null ? 'bg-danger' : '' }}">
                                <td>{{ $value->id }}</td>
                                <td>{{ !empty($value->full_name) ? ucwords(strtolower($value->full_name)) : 'N/A' }}</td>
                                <td>
                                    @if(!empty( $value->dan_list ))
                                        @if(!empty( $value->dan_list->dan ))
                                            {{ $value->dan_list->dan }}
                                        @else
                                            N/A
                                        @endif
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route( 'view-instructor' , [ 'id' => $value->id ] ) }}" role="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i><span></span> View</a>
                                    @if($value->deleted_at == null)
                                        <a href="{{ route( 'edit-instructor' , [ 'id' => $value->id ] ) }}" role="button" class="btn btn-success btn-sm" id="edit_{{$value->id}}"><i class="fa fa-pencil"></i><span></span> Edit</a>
                                        <button class="btn btn-danger btn-sm" onclick="deleteData({{ $value->id }})" type="submit" id="delete_{{$value->id}}"><i class="fa fa-trash-o"></i><span></span> Delete</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>Empty</td>
                            <td>Empty</td>
                            <td>Empty</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        
        <div class="container" style="width: 90%;">
            <div class="clearfix"></div>
{{--            {{ $values->links() }}--}}
        </div>
        
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- METISMENU SCRIPTS -->
<script src="{{ asset('assets/js/jquery.metisMenu.js') }}"></script>
<!-- CUSTOM SCRIPTS -->
{{--<script src="{{ asset('assets/js/custom.js') }}"></script>--}}


<link href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteData(id){
        console.log(id);
        var token = $('meta[name="csrf-token"]').attr('content');
        console.log(token);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url : "{{route('admin.dashboard')}}" + '/instructor/' + id ,
                        type: 'POST',
                        data : {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token
                        },
                        success: function(){
                            swal({
                                    title: "Success!",
                                    text : "Post has been deleted \n Click OK to refresh the page",
                                    icon : "success",
                                });
                            document.getElementById("edit_"+id).remove();
                            document.getElementById("delete_"+id).remove();
                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                    swal("Your imaginary file is safe!");
                }
            });
    }

    ( function($){

        $(document).ready(function() {
            $('#example').DataTable();
        } );

    }) (jQuery);




</script>

</body>
</html>
