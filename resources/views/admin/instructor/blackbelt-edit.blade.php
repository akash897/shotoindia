@include('admin.layouts.nav')

@include('admin.layouts.side-nav')
<div id="wrapper">
    <div id="page-wrapper" >
        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Edit Black Belt</div>

                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('update-blackbelt',['id' => $user->id]) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Full Name</label>

                                    <div class="col-md-6">
                                        <input id="fullname" type="text" class="form-control" name="fullname" value="{{ $user->full_name }}" required>

                                        @if ($errors->has('fullname'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('fullname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('dan') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Dan</label>

                                    <div class="col-md-6">
                                        <select class="form-control" id="dan" name="dan">
                                            <option value="">--- SELECT ---</option>
                                            @foreach($dan_list as $value)
                                                @if(!empty($value))
                                                    <option value="{{ $value->id }}" {{ $value->id == $user->dan_list_id ? 'selected' : '' }}>{{ $value->dan }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                                    <label for="year" class="col-md-4 control-label">Year</label>

                                    <div class="col-md-6">
                                        <select class="form-control" id="year" name="year">
                                            <option value="">--- SELECT ---</option>
                                            @foreach($year as $y)
                                                    <option value="{{ $y }}" {{ $y == $user->year ? 'selected' : '' }}>{{ $y }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('instructor') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Instructor</label>

                                    <div class="col-md-6">
                                        <select class="form-control" id="instructor" name="instructor" required>
                                            <option value="">--- SELECT ---</option>
                                            <option value="yes" {{ $user->instructor == 'yes' ? 'selected' : '' }}>Yes</option>
                                            <option value="no" {{ $user->instructor == 'no' ? 'selected' : '' }}>No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Image</label>

                                    <div class="col-md-6">
                                        <input id="name" type="file" class="form-control" name="name"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- METISMENU SCRIPTS -->
<script src="{{ asset('assets/js/jquery.metisMenu.js') }}"></script>
<!-- CUSTOM SCRIPTS -->
<script src="{{ asset('assets/js/custom.js') }}"></script>


</body>
</html>
