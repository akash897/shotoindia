@include('admin.layouts.nav')
<style>
    .profile-image{
        height: 150px;
        width: 150px;
    }
</style>
<div id="wrapper">
    @include('admin.layouts.side-nav')
    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2>Black Belt User</h2>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <img class="profile-image img-responsive img-modal" src="{{ !empty($value->image_url) ? asset($value->image_url) : asset('images/black-belt/user-icon.png') }}">
{{--                    <button type="button" class="btn btn-danger btn-block">Upload</button>--}}
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Name
                    </div>
                    <div class="col-md-6">
                        {{ !empty($value->full_name) ? ucwords(strtolower($value->full_name)) : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Dan
                    </div>
                    <div class="col-md-6">
                        {{ !empty($value->dan_list) ? $value->dan_list->dan : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Year
                    </div>
                    <div class="col-md-6">
                        {{ !empty($value->year) ? $value->year : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Created By
                    </div>
                    <div class="col-md-6">
                        {{ !empty($value->created_by) ? $value->created_by : 'System Generated' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Modified By
                    </div>
                    <div class="col-md-6">
                        {{ !empty($value->modified_by) ? $value->modified_by : 'System Generated' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Created At
                    </div>
                    <div class="col-md-6">
                        {{ !empty($value->created_at) ? $value->created_at : 'System Generated' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Updated at
                    </div>
                    <div class="col-md-6">
                        {{ !empty($value->updated_at) ? $value->updated_at : 'System Generated' }}
                    </div>
                </div>
                @if($value->deleted_at != null)
                    <div class="clearfix"></div>
                    <div class="col-md-6 pull-right">
                        <div class="text-right mb-3">
                            <a href="{{ route('recovery-blackbelt',['id'=> $value->id]) }}" class="btn btn-primary">Recover</a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row">


            </div>

        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>


</body>
</html>
