@include('admin.layouts.nav')

@include('admin.layouts.side-nav')
<div id="wrapper">
    <div id="page-wrapper" >
        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add Black Belt</div>

                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('store-blackbelt') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Full Name</label>

                                    <div class="col-md-6">
                                        <input id="fullname" type="text" class="form-control" name="fullname" value="{{ old('fullname') }}" required>

                                        @if ($errors->has('fullname'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('fullname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('dan') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Dan</label>

                                    <div class="col-md-6">
                                        <select class="form-control" id="dan" name="dan" required val="{{ old('dan') }}">
                                            <option value="">--- SELECT ---</option>
                                            <option value="1">Black Belt 1st Dan</option>
                                            <option value="2">Black Belt 2nd Dan</option>
                                            <option value="3">Black Belt 3rd Dan</option>
                                            <option value="4">Black Belt 4th Dan</option>
                                            <option value="5">Black Belt 5th Dan</option>
                                            <option value="6">Black Belt 6th Dan</option>
                                            <option value="7">Black Belt 7th Dan</option>
                                            <option value="8">Black Belt 8th Dan</option>
                                            <option value="9">Black Belt 9th Dan</option>
                                            <option value="10">Black Belt 10th Dan</option>
                                        </select>

                                        @if ($errors->has('dan'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dan') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                                    <label for="year" class="col-md-4 control-label">Year</label>

                                    <div class="col-md-6">
                                        <select class="form-control" id="year" name="year">
                                            <option value="">--- SELECT ---</option>
                                            @foreach($year as $y)
                                                <option value="{{ $y }}">{{ $y }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Image</label>

                                    <div class="col-md-6">
                                        <input id="name" type="file" class="form-control" name="name"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- METISMENU SCRIPTS -->
<script src="{{ asset('assets/js/jquery.metisMenu.js') }}"></script>
<!-- CUSTOM SCRIPTS -->
<script src="{{ asset('assets/js/custom.js') }}"></script>


</body>
</html>
