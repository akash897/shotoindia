@include('admin.layouts.nav')

@include('admin.layouts.side-nav')
<div id="wrapper">
    <div id="page-wrapper" >
        <div class="container col-md-12" style="margin-top: 50px;">
            @if(!empty($events))
                @foreach($events as $key => $event)
                    <a href="{{ route('admin.album.uploading',['id'=>$event->id]) }}" >{{$event->name}}</a> <br>
                @endforeach
            @else
             no data
            @endif

        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- METISMENU SCRIPTS -->
<script src="{{ asset('assets/js/jquery.metisMenu.js') }}"></script>
<!-- CUSTOM SCRIPTS -->
<script src="{{ asset('assets/js/custom.js') }}"></script>


</body>
</html>
