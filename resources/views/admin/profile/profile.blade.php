@include('admin.layouts.nav')

<div id="wrapper">
    @include('admin.layouts.side-nav')
    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2>Profile</h2>
                    <h5>Welcome {{ Auth::user()->name }}</h5>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr>
            <div class="row">
                <div class="col-md-12">
                        <div class="panel-heading">
                            Profile Complete
                        </div>

                        <div class="panel-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-success active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    40% Complete (success)
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-6">
                        Name
                    </div>
                    <div class="col-md-6">
                        {{ !empty(Auth::user()->name) ? Auth::user()->name : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Email
                    </div>
                    <div class="col-md-6">
                        {{ !empty(Auth::user()->email) ? Auth::user()->email : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Phone Number
                    </div>
                    <div class="col-md-6">
                        {{ !empty(Auth::user()->phone_no) ? Auth::user()->phone_no : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Created By
                    </div>
                    <div class="col-md-6">
                        {{ !empty(Auth::user()->created_by) ? Auth::user()->created_by : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Modified By
                    </div>
                    <div class="col-md-6">
                        {{ !empty(Auth::user()->modified_by) ? Auth::user()->modified_by : 'N/A' }}
                    </div>
                </div>
            </div>
            <!-- /. ROW  -->
            <br>
            <hr>
            <br>
            <!-- /. ROW  -->
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-6">
                        Device
                    </div>
                    <div class="col-md-6">
                        @if($agent_data->is_mobile)
                            Mobile
                        @endif
                        @if($agent_data->is_tablet)
                            Tablet
                        @endif
                        @if($agent_data->is_desktop)
                            Desktop
                        @endif
                        @if($agent_data->is_iphone && $agent_data->platform != 'AndroidOS')
                                iPhone
                        @endif
                        @if($agent_data->is_robot)
                            Robot
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Detecting Language
                    </div>
                    <div class="col-md-6">
                        {{ !empty($agent_data->languages[2]) ? mb_strtoupper($agent_data->languages[2]) : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Device
                    </div>
                    <div class="col-md-6">
                        {{ !empty($agent_data->device) ? $agent_data->device : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Robot Name
                    </div>
                    <div class="col-md-6">
                        {{ !empty($agent_data->robot_name) ? $agent_data->robot_name : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Browser
                    </div>
                    <div class="col-md-6">
                        {{ !empty($agent_data->browser) ? $agent_data->browser : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        {{ !empty($agent_data->browser) ? $agent_data->browser : 'Browser' }} Version
                    </div>
                    <div class="col-md-6">
                        {{ !empty($agent_data->browser_version) ? $agent_data->browser_version : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        Operating System
                    </div>
                    <div class="col-md-6">
                        {{ !empty($agent_data->platform) ? $agent_data->platform : 'N/A' }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        {{ !empty($agent_data->platform) ? $agent_data->platform : 'Operating System' }} Version
                    </div>
                    <div class="col-md-6">
                        {{ !empty($agent_data->platform_version) ? $agent_data->platform_version : 'N/A' }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>


</body>
</html>
