<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li class="text-center">
                <img src="{{ asset('assets/img/find_user.png') }}" class="user-image img-responsive"/>
            </li>


<!--            <li>
                <a  href="index.html"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
            </li>
            <li>
                <a  href="ui.html"><i class="fa fa-desktop fa-3x"></i> UI Elements</a>
            </li>
            <li>
                <a  href="tab-panel.html"><i class="fa fa-qrcode fa-3x"></i> Tabs & Panels</a>
            </li>
            <li  >
                <a  href="chart.html"><i class="fa fa-bar-chart-o fa-3x"></i> Morris Charts</a>
            </li>
            <li  >
                <a  href="table.html"><i class="fa fa-table fa-3x"></i> Table Examples</a>
            </li>
            <li  >
                <a  href="form.html"><i class="fa fa-edit fa-3x"></i> Forms </a>
            </li>


            <li>
                <a href="#"><i class="fa fa-sitemap fa-3x"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Second Level Link</a>
                    </li>
                    <li>
                        <a href="#">Second Level Link</a>
                    </li>
                    <li>
                        <a href="#">Second Level Link<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>

                        </ul>

                    </li>
                </ul>
            </li>
            <li  >
                <a class=""  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
            </li>-->

{{--            @if(Auth::super_admin())--}}
{{--                <li>--}}
{{--                    <a  href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>--}}
{{--                </li>--}}
{{--            @endif--}}
            
            <li>
                <a  href="{{ route('admin.profile') }}"><i class="fa fa-user fa-3x"></i> Profile</a>
            </li>
            
            <li>
                <a  href="{{ route('blackbelt-list') }}"><i class="fa fa-edit fa-3x"></i> Black Belts</a>
            </li>

            <li>
                <a  href="{{ route('instructor-list') }}"><i class="fa fa-edit fa-3x"></i> Instructor</a>
            </li>

{{--            @if(Auth::super_admin())--}}
{{--                <li>--}}
{{--                    <a  href="{{ route('admin.event') }}"><i class="fa fa-adjust fa-3x"></i> Events</a>--}}
{{--                </li>--}}
{{--            @endif--}}
        </ul>

    </div>

</nav>
<!-- /. NAV SIDE  -->