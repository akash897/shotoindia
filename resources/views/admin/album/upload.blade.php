@include('admin.layouts.nav')

@include('admin.layouts.side-nav')
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<link href="{{ asset('assets/css/dropzone/dropzone.css') }}" rel="stylesheet" />
<script>


    Dropzone.options.dropzoneElement = {
        init: function() {

            this.on("sending", function(file, xhr, formData) {
                formData.append("status", 'new');
                formData.append("user_id", 1);
            });


            this.on("success", function(file, responseText) {
                alert('Success');
            });
        }
    };

</script>
<div id="wrapper">
    <div id="page-wrapper" >
        <div class="container col-md-12" style="margin-top: 50px;">
            {{ $event->name }}
            <div class="clearfix"></div>
            <br><br>

            <div class="">
                <div id="dropzone">
                    <form name="file[]" action="{{ route('admin.album.uploading',['id'=>$event->id]) }}" class="dropzone needsclick dz-clickable" id="#album_upload">
{{--                    <form action="{{ route('admin.album.uploading',['id'=>$event->id]) }}" method="POST" name="file[]" class="dropzone needsclick dz-clickable" id="#album_upload">--}}
                        {{ csrf_field() }}
                        <div class="dz-message needsclick">
                            Drop files here or click to upload.<br>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- METISMENU SCRIPTS -->
<script src="{{ asset('assets/js/jquery.metisMenu.js') }}"></script>
<!-- CUSTOM SCRIPTS -->
<script src="{{ asset('assets/js/custom.js') }}"></script>


</body>
</html>
