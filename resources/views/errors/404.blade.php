@if(!Auth::user())
    <html lang="zxx">

    <head>
        <title>Page not found</title>
        <!-- custom-theme -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Gym Trainer Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript">
            addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
                window.scrollTo(0, 1);
            }
        </script>
        <!-- //custom-theme -->
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
        <!-- Owl-carousel-CSS -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
        <!-- font-awesome-icons -->
        <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
        <!-- //font-awesome-icons -->
        <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
    </head>

    <body>
    <!-- banner -->
    <div class="main_section_agile inner">
        {{--    @include('auth.layouts.navbar')--}}
    </div>

    <!-- //banner -->
    <!---728x90--->
    <!-- about inner -->
@else
    @include('admin.layouts.nav')
    <br><br>
@endif
<div class="about-bottom inner-padding">
    <div class="container">
        <div class="wthree_head_section">
            <h3 class="w3l_header">404</h3>
        </div>
        <div class="about-bott-right">
            <h5>Page Not Found</h5>
            <p>The link you followed may be broken, or the page may have been removed. The requested URL {{ url()->current() }} was not found on this server</p>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
@if(!Auth::user())

    <!-- footer -->
    {{--@include('auth.layouts.footer')--}}
    <!-- //footer -->
    <!-- js -->
    <script type="text/javascript" src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
    <!-- //js -->
    <script type="text/javascript" src="{{ asset('js/bootstrap-3.1.1.min.js') }}"></script>
    <!-- for testimonials slider-js-script-->

    </body>

    </html>
@else
    <!-- /. WRAPPER  -->

    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>


    </body>
    </html>
@endif